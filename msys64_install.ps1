#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
param([string]$DRIVE = "DRIVE", [string]$FOLDER = "FOLDER", [string]$VERSION = "VERSION",  [string]$VENDOR = "VENDOR" );
#Read-Host -Prompt "Pausing:  Press any key to continue"

$EICON_OPENOCD_RELEASE     = "0.11.0"
$DINRDUINO_PATH            = "C:\DinRDuino"
$MSYS2_LATEST              = "http://repo.msys2.org/distrib/msys2-x86_64-latest.tar.xz"
$7ZIP_PATH                 = "C:\PgmFiles\7-Zip"
$ZIP_PATH                  = "${DINRDUINO_PATH}\dload\msys2-x86_64-latest.tar.xz"
$TAR_PATH                  = "${DINRDUINO_PATH}\dload\msys2-x86_64-latest.tar"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    
    Write-Host "Line $(CurrentLine)   DinRDuino PATH             = ${DINRDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    echo_args                                                                        # Calling echo_args

    Write-Host "Line $(CurrentLine)   Executing                    cd ${DINRDUINO_PATH}";
    cd ${DINRDUINO_PATH}
    Write-Host "Line $(CurrentLine)   PWD                        = cd ${PWD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)   Calling                      install_msys64";
    install_msys64

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}





function install_msys64
{
    Write-Host "Line $(CurrentLine)   Entering                     echo_args";
    Write-Host "Line $(CurrentLine)   Installing                   MSYS2..." -ForegroundColor Cyan

#    Set-Content -Value "Write-Host 'Sleep then kill gpg-agent.exe'; sleep 300; Stop-Process -name gpg-agent -Force" -Path .\kill-gpg-agent.ps1
#    Start-Process powershell.exe -ArgumentList .\kill-gpg-agent.ps1

    Write-Host "Line $(CurrentLine)   Executing                    if(Test-path ${DINRDUINO_PATH}\msys64)";            
    if(Test-path ${DINRDUINO_PATH}\msys64)
	{
        Write-Host "Line $(CurrentLine)   Executing                    del ${DINRDUINO_PATH}\msys64 -Recurse -Force";            
        del ${DINRDUINO_PATH}\msys64 -Recurse -Force
    }
	else
	{
        Write-Host "Line $(CurrentLine)   ${DINRDUINO_PATH}\msys64          does not exist   continuing";            
	}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # download installer
    Write-Host "Line $(CurrentLine)   Executing                    if(Test-path ${ZIP_PATH})";            
    if(Test-path ${ZIP_PATH})
	{
		Write-Host "Line $(CurrentLine)  Msys64 Latest                exists ... continuing";
	}
	else
	{
        Write-Host "Line $(CurrentLine)  Downloading                  MSYS installation package...";		
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${MSYS2_LATEST} -Outfile ${ZIP_PATH}";            
        Invoke-WebRequest -Uri ${MSYS2_LATEST} -Outfile ${ZIP_PATH}
    }
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Untaring                     installation package..."
	Write-Host "Line $(CurrentLine)  Executing                    & ${7ZIP_PATH}\7z.exe x ${ZIP_PATH} -y -o"${TAR_PATH}" | Out-Null";            
    &${7ZIP_PATH}\7z.exe x ${ZIP_PATH} -y -o"${TAR_PATH}" | Out-Null

    Write-Host "Line $(CurrentLine)  Unzipping                    installation package..."
    Write-Host "Line $(CurrentLine)  Executing                    & ${7ZIP_PATH}\7z.exe x ${TAR_PATH} -y -o"${DINRDUINO_PATH}" | Out-Null"
    & ${7ZIP_PATH}\7z.exe x ${TAR_PATH} -y -o"${DINRDUINO_PATH}" | Out-Null

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Executing                    [Environment]::SetEnvironmentVariable("MSYS2_PATH_TYPE", "inherit", "Machine")"
    [Environment]::SetEnvironmentVariable("MSYS2_PATH_TYPE", "inherit", "Machine")
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    #install latest pacman
    bash 'pacman -Sy --noconfirm pacman pacman-mirrors'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # update core packages
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Syuu --needed --noconfirm --ask=20'"
    bash 'pacman -Syuu --needed --noconfirm --ask=20'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Syu --noconfirm'"
    bash 'pacman -Syu --noconfirm'
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Syu --noconfirm'"
    bash 'pacman -Syu --noconfirm'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # install packages
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm VCS'"
    bash 'pacman --sync --noconfirm VCS'
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm base-devel'"
    bash 'pacman --sync --noconfirm base-devel'
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm msys2-devel'"
    bash 'pacman --sync --noconfirm msys2-devel'
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-{x86_64,i686}-toolchain'"
    bash 'pacman --sync --noconfirm mingw-w64-{x86_64,i686}-toolchain'

    # cleanup pacman cache
    Write-Host "Line $(CurrentLine)  Executing                    Remove-Item ${DINRDUINO_PATH}\msys64\var\cache\pacman\pkg -Recurse -Force"
    Remove-Item ${DINRDUINO_PATH}\msys64\var\cache\pacman\pkg -Recurse -Force

    # add mapping for .sh files
    Write-Host "Line $(CurrentLine)  Executing                    cmd /c ftype sh_auto_file="${DINRDUINO_PATH}\msys64\usr\bin\bash.exe" "`"%L`"" %* | out-null"
    cmd /c ftype sh_auto_file="${DINRDUINO_PATH}\msys64\usr\bin\bash.exe" "`"%L`"" %* | out-null
    Write-Host "Line $(CurrentLine)  Executing                    cmd /c assoc .sh=sh_auto_file"
    cmd /c assoc .sh=sh_auto_file

    # compact folder
    Write-Host "Line $(CurrentLine)  Compacting                  ${DINRDUINO_PATH}\msys64..." -NoNewline
    compact /c /i /s:${DINRDUINO_PATH}\msys64 | Out-Null
    Write-Host "  - OK" -ForegroundColor Green

#    Write-Host "Line $(CurrentLine)  Executing                    Remove-Item .\kill-gpg-agent.ps1 -Force -ErrorAction Ignore"
#    Remove-Item .\kill-gpg-agent.ps1 -Force -ErrorAction Ignore

    Write-Host "MSYS2 installed" -ForegroundColor Green
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host -NoNewline "Line $(CurrentLine)  Arguments                    ";
    Write-Host -NoNewline "DRIVE = $DRIVE, ";
    Write-Host -NoNewline "FOLDER = $FOLDER, ";
    Write-Host -NoNewline "VERSION = $VERSION, ";
    Write-Host -NoNewline "VENDOR = $VENDOR, ";
    Write-Host "";
    Write-Host "Line $(CurrentLine)  EICON_OPENOCD_RELEASE      = ${EICON_OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  MSYS2_LATEST               = $MSYS2_LATEST";	
    Write-Host "Line $(CurrentLine)  DINRDUINO_PATH             = ${DINRDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  7ZIP_PATH                  = ${7ZIP_PATH}";
    Write-Host "Line $(CurrentLine)  ZIP_PATH                   = ${ZIP_PATH}";
    Write-Host "Line $(CurrentLine)  TAR_PATH                   = ${TAR_PATH}";
	
    Write-Host "Line $(CurrentLine)  USER                       = $env:UserName";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
}

function bash($command)
{
    Write-Host "Line $(CurrentLine)  Executing                    $command" -NoNewline
    cmd /c start /wait ${DINRDUINO_PATH}\msys64\usr\bin\sh.exe --login -c $command
    Write-Host " - OK" -ForegroundColor Green
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}

#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#---------------------------------------------------------
# testing $PATH
# C:\msys64\usr\bin\sh.exe -lc "echo $PATH"
# C:\msys64\usr\bin\bash.exe -lc "echo $PATH"

#C:\msys64\usr\bin\sh.exe -lc "pacman -Sy --noconfirm pacman pacman-mirrors"
#C:\msys64\usr\bin\sh.exe -lc "pacman -Syu --noconfirm"
#C:\msys64\usr\bin\sh.exe -lc "pacman -Syu --noconfirm"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm VCS"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm base-devel"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm msys2-devel"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm mingw-w64-{x86_64,i686}-toolchain"
#---------------------------------------------------------
