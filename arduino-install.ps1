#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#param([string]$DRIVE = "DRIVE", [string]$FOLDER = "FOLDER", [string]$VERSION = "VERSION", [string]$VENDOR = "VENDOR", [string]$BOARD = "BOARD",
#      [string]$CPU = "CPU", [string]$SKETCH = "SKETCH", [string]$SKETCH_VER = "SKETCH_VER", [string]$ARCHITECTURE = "ARCHITECTURE",
param([string]$DRIVE = "DRIVE", [string]$FOLDER = "FOLDER", [string]$VERSION = "VERSION",  [string]$VENDOR = "VENDOR" );
#      [string]$CPU = "CPU", [string]$SKETCH = "SKETCH", [string]$SKETCH_VER = "SKETCH_VER", [string]$ARCHITECTURE = "ARCHITECTURE") ;

#param([string]$DRIVE = "DRIVE" );
#Read-Host -Prompt "Pausing:  Press any key to continue"

$OPENOCD_RELEASE           = "0.11.0"
$EICON_SKETCHBOOK_RELEASE  = "v0.1.1"
$ARDUINO_CLI_RELEASE       = "v0.1.0"
$BASE_PATH                 = "${DRIVE}:\$FOLDER"
$ARDUINO_PATH              = "${BASE_PATH}\arduino-$VERSION"
$PORTABLE_PATH             = "${ARDUINO_PATH}\portable"
$HARDWARE_PATH             = "${PORTABLE_PATH}\packages\${VENDOR}\hardware"
$OPENOCD_PATH              = "${BASE_PATH}\Openocd"
$DLOAD_PATH                = "${BASE_PATH}\dload"
$PREFS_PATH                = "${PORTABLE_PATH}"
$SKETCH_PATH               = "${PORTABLE_PATH}\sketchbook\arduino"
$SKETCH_PREFS_PATH         = "${SKETCH_PATH}\${SKETCH_NAME}\preferences"
$ARDUINO_ZIP_URI           = "https://downloads.arduino.cc/arduino-${VERSION}-windows.zip"
$OPENOCD_URI               = "https://gitlab.com/noeldiviney/eicon-openocd/-/archive/${OPENOCD_RELEASE}/eicon-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_URL           = "https://downloads.arduino.cc/arduino-cli/arduino-cli_latest_Windows_64bit.zip"
$STM32_INDEX               = "        https://gitlab.com/noeldiviney/eicon_arduino_projects/-/raw/main/package_eicon_stm32_index.json,"
$KINETIS_INDEX             = "        https://gitlab.com/noeldiviney/eicon_arduino_projects/-/raw/main/package_eicon_kinetis_index.json"
$ARDUINO_ZIP_FILE          = "arduino-${VERSION}.zip"
$SKETCH_NAME               = "${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                      = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH              = "/home/$env:USER"
$SCRIPT_PATH               = "${DRIVE}:\bin\pwshell"
$OPENOCD_ZIP_FILE          = "eicon-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_URI           = "https://gitlab.com/noeldiviney/arduino_cli/-/archive/${ARDUINO_CLI_RELEASE}/arduino_cli-${ARDUINO_CLI_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE      = "arduino_cli.zip"
$SKETCHBOOK_VERSION        = "eiconsketchbook-v0.1.1"
$SKETCHBOOK_ZIP_URI        = "https://gitlab.com/noeldiviney/eiconsketchbook/-/archive/${EICON_SKETCHBOOK_RELEASE}/eiconsketchbook-${EICON_SKETCHBOOK_RELEASE}.zip"
$SKETCHBOOK_ZIP_FILE       = "sketchbook.zip"
$SourceFileLocation        = "C:\Program Files\Notepad++\notepad++.exe"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    
    Write-Host "Line $(CurrentLine)   BASE_PATH                  = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)   Executing                    cd ${BASE_PATH}";
    

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Returning from               echo_args";
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";

    Write-Host "Line $(CurrentLine)   Calling                      create_${BASE_PATH}";
    create_${BASE_PATH}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_arduino-${VERSION}";
    install_arduino-${VERSION}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_openocd-${OPENOCD_VERSION}"
    install_openocd-${OPENOCD_VERSION}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cli"
    install_arduino_cli
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cli_config"
    install_arduino_cli_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      core_update-index"
    core_update-index
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      core_install_stm32"
    core_install_stm32
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      core_install_kinetis"
    core_install_kinetis
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_sketchbook"
    install_sketchbook
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_DesktopShortcut"       # Work in Profress !!!
#    install_DesktopShortcut

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_DesktopShortcut
#---------------------------------------------------------
function install_DesktopShortcut
{
#$WScriptShell = New-Object -ComObject WScript.Shell
    Write-Host "Line $(CurrentLine)  Entering                     install_DesktopShortcut";
#    $SourceFileLocation = "C:\Program Files\PowerShell\7\pwsh.exe"
#    $ShortcutLocation = "C:\Users\$USER\Desktop\pwsh.exe.lnk"
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    $Shortcut = "$WScriptShell.CreateShortcut($ShortcutLocation)" 
#    $Shortcut.TargetPath = $SourceFileLocation "C:\Program Files\PwrShell\arduino-install.ps1" "W Dinrduino5 1.8.15 Eicon"
#    $Shortcut.Save()
#    Write-Host "Line $(CurrentLine)  Executing                 $ShortcutLocation = "C:\Users\$USER\Desktop\Notepad++.lnk";
#    $ShortcutLocation = "C:\Users\$USER\Desktop\Notepad++.lnk"
#    Write-Host "Line $(CurrentLine)  Executing                 $WScriptShell = New-Object -ComObject WScript.Shell";                 
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    Write-Host "Line $(CurrentLine)  Executing                 '$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)'";
#    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.TargetPath = $SourceFileLocation";
#    $Shortcut.TargetPath = $SourceFileLocation
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.Save()";
#    $Shortcut.Save()
    Write-Host "Line $(CurrentLine)  Leaving                      install_DesktopShortcut";
}
#---------------------------------------------------------
# install_sketchbook
#---------------------------------------------------------
function install_sketchbook
{
    Write-Host "Line $(CurrentLine)  Entering                     install_sketchbook";
    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}\dload\${SKETCHBOOK_ZIP_FILE}";
    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}\dload\${SKETCHBOOK_ZIP_FILE}
    if (Test-Path ${PORTABLE_PATH}\sketchbook)
    {
        Write-Host "Line $(CurrentLine)  sketchbook fo;der              Exists , Deleting";
        Remove-Item ${PORTABLE_PATH}\sketchbook
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}\dload\${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}\";
    Expand-Archive -Force -PATH ${BASE_PATH}\dload\${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}\ 
    Write-Host "Line $(CurrentLine)  Renaming                     ${PORTABLE_PATH}\${SKETCHBOOK_VERSION}";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}\sketchbook";
    Rename-Item ${PORTABLE_PATH}\${SKETCHBOOK_VERSION} ${PORTABLE_PATH}\sketchbook
    Write-Host "Line $(CurrentLine)  Entering                     install_sketchbook";
}
#---------------------------------------------------------
# core_install_kinetis
#---------------------------------------------------------
function core_install_kinetis
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_kinetis";
    if (Test-Path ${HARDWARE_PATH}\kinetis)
    {
        Write-Host "Line $(CurrentLine)  kinetis_core                   Exists , Deleting";
        Remove-Item ${HARDWARE_PATH}\kinetis
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli.exe core install ${VENDOR}:kinetis";
    & $PORTABLE_PATH\arduino-cli.exe core install ${VENDOR}:kinetis    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_kinetis";
}

#---------------------------------------------------------
# core_install_stm32
#---------------------------------------------------------
function core_install_stm32
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stm32";
    if (Test-Path ${HARDWARE_PATH}\stm32)
    {
        Write-Host "Line $(CurrentLine)  stm32_core                   Exists , Deleting";
        Remove-Item ${HARDWARE_PATH}\stm32
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli.exe core install ${VENDOR}:stm32";
    & $PORTABLE_PATH\arduino-cli.exe core install ${VENDOR}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_stm32";
}

#---------------------------------------------------------
# core_update-index
#---------------------------------------------------------
function core_update-index
{
    Write-Host "Line $(CurrentLine)  Entering                     core_update-index";
    Write-Host "Line $(CurrentLine)  Executing                    cd ${PORTABLE_PATH}";
    cd ${PORTABLE_PATH}
    Write-Host "Line $(CurrentLine)  Current Working Dir        = $PWD";
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli.exe core update-index";
    & $PORTABLE_PATH\arduino-cli.exe core update-index    
    Write-Host "Line $(CurrentLine)  Leaving                      core_update-index";
}
#---------------------------------------------------------
# install_arduino_cli_config
#---------------------------------------------------------
function install_arduino_cli_config
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli_config";

if (Test-Path ${PORTABLE_PATH}\arduino-cli.yaml)
    {
        Write-Host "Line $(CurrentLine)     arduino-cli.yaml Exists , Deleting";
        Remove-Item ${PORTABLE_PATH}\arduino-cli.yaml
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}\arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}\arduino-cli.yaml
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}\arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}\arduino-cli.yaml
    }    
    Write-Host "Line $(CurrentLine)  Adding                       arduino-cli.yaml Contents";
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml 'board_manager:'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    additional_urls: ['
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml          ${STM32_INDEX}
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml          ${KINETIS_INDEX}
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    ]'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml 'daemon:'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    port: "50051"'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml 'directories:'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    data: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml             ${PORTABLE_PATH}
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    downloads: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml             ${PORTABLE_PATH}\staging
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    user: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml             ${PORTABLE_PATH}\Workspace
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml 'logging:'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    file: ""'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    format: text'
    Add-Content ${PORTABLE_PATH}\arduino-cli.yaml '    level: info'
    Write-Host "Line $(CurrentLine)  Editing                      arduino-cli.yaml Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli_config";
}
#---------------------------------------------------------
# install_arduino_cli
#---------------------------------------------------------
function install_arduino_cli
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE})";
    if (Test-Path ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE})
    {
        Write-Host "Line $(CurrentLine)     ${ARDUINO_CLI_ZIP_FILE}) Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE})";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE})";
        Invoke-WebRequest -Uri ${ARDUINO_CLI_URL} -Outfile ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE}
    }
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE} -Destination ${PORTABLE_PATH}";
    Expand-Archive -Force -PATH ${BASE_PATH}\dload\${ARDUINO_CLI_ZIP_FILE} -Destination ${PORTABLE_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli";
}
#---------------------------------------------------------
# install_openocd-${OPENOCD_VERSION}
#---------------------------------------------------------
function install_openocd-${OPENOCD_VERSION}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd-${OPENOCD_VERSION}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE})";
    if (Test-Path ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE})
    {
        Write-Host "Line $(CurrentLine)     ${OPENOCD_ZIP_FILE}) Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE})";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_URI} -Outfile ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE})'";
        Invoke-WebRequest -Uri ${OPENOCD_URI} -Outfile ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE}
#Read-Host -Prompt "Pausing:  Press any key to continue"
    }
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE} -Destination ${BASE_PATH}";
    Expand-Archive -Force -PATH ${BASE_PATH}\dload\${OPENOCD_ZIP_FILE} -Destination ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd-${OPENOCD_VERSION}";
}

#---------------------------------------------------------
# install_arduino-${VERSION}
#---------------------------------------------------------
function install_arduino-${VERSION}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino-${VERSION}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}\dload\${ARDUINO_ZIP_FILE})";
    if (Test-Path ${BASE_PATH}\dload\${ARDUINO_ZIP_FILE})
    {
        Write-Host "Line $(CurrentLine)  arduino-${VERSION}.zip           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}\dload\${ARDUINO_ZIP_FILE}";
        Invoke-WebRequest -Uri ${ARDUINO_ZIP_URI} -Outfile ${BASE_PATH}\dload\${ARDUINO_ZIP_FILE}
    }
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Force -PATH ${BASE_PATH}\${ARDUINO_ZIP_FILE} -Destination ${BASE_PATH}\";
    Expand-Archive -Force -PATH ${BASE_PATH}\dload\${ARDUINO_ZIP_FILE} -Destination ${BASE_PATH}\
    
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino-${VERSION}";
}
#---------------------------------------------------------
# create_${BASE_PATH}
#---------------------------------------------------------
function create_${BASE_PATH}
{
    Write-Host "Line $(CurrentLine)  Entering                     create_${BASE_PATH}";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  ${BASE_PATH}                 Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                      create_${BASE_PATH}";
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host -NoNewline "Line $(CurrentLine)  Arguments                    ";
    Write-Host -NoNewline "DRIVE = $DRIVE, ";
    Write-Host -NoNewline "FOLDER = $FOLDER, ";
    Write-Host -NoNewline "VERSION = $VERSION, ";
    Write-Host -NoNewline "VENDOR = $VENDOR, ";
#	Write-Host "BOARD = $BOARD"
#    Write-Host -NoNewline "          Arguments Contd           CPU = $CPU, "
#	Write-Host -NoNewline "SKETCH = $SKETCH, "
#    Write-Host -NoNewline "SKETCH_VER = $SKETCH_VER, "
#    Write-Host "ARCHITECTURE = $ARCHITECTURE, "
#    Write-Host "          Arguments Contd           PROTOCOL = ${PROTOCOL} "
    Write-Host "";
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE            = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  EICON_SKETCHBOOK_RELEASE   = ${EICON_SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE        = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH               = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH              = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH              = ${HARDWARE_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                 = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PREFS_PATH          = ${SKETCH_PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_URI            = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE           = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH               = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_URI         = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE        = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  USER                       = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 $ARDUINO_PATH\arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH\arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

